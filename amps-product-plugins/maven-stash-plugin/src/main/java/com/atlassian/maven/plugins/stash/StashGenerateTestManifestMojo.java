package com.atlassian.maven.plugins.stash;

import com.atlassian.maven.plugins.amps.osgi.GenerateTestManifestMojo;

import org.apache.maven.plugins.annotations.Mojo;

@Mojo(name = "generate-test-manifest")
public class StashGenerateTestManifestMojo extends GenerateTestManifestMojo
{
}
